# ZTE F680 DOCUMENTATION 
Unofficial documentation about F680 

## Virgin v4

This files `zte-f680-v4-virgin.txt` and `zte-f680-v4-virgin.bin` are the results of dumping the NAND using the CFE to console and capture the output. Then I used `xdd` to extract a binary :

```
cat zte-f680-v4-virgin.txt | sed -n '/^---/,/^---/ { /^[-]/d;  p}' | xxd -r > zte-f680-v4-virgin.bin
```


bootlogs files, rootfs is selected at CFE with 'c' option 'change boot parameters':

* zte-f680-v4-virgin-rootfs1-bootlog.txt
* zte-f680-v4-virgin-rootfs2-bootlog.txt

parts:
- 29FIG08ABAEA: NAND
 https://www.micron.com/products/nand-flash/serial-nand/part-catalog/mt29f2g01abagdwb-it
